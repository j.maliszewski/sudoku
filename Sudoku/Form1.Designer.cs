﻿namespace Sudoku
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.button82 = new System.Windows.Forms.Button();
            this.button78 = new System.Windows.Forms.Button();
            this.button35 = new System.Windows.Forms.Button();
            this.button33 = new System.Windows.Forms.Button();
            this.button32 = new System.Windows.Forms.Button();
            this.button76 = new System.Windows.Forms.Button();
            this.button80 = new System.Windows.Forms.Button();
            this.button65 = new System.Windows.Forms.Button();
            this.button79 = new System.Windows.Forms.Button();
            this.button75 = new System.Windows.Forms.Button();
            this.button77 = new System.Windows.Forms.Button();
            this.button74 = new System.Windows.Forms.Button();
            this.button66 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button64 = new System.Windows.Forms.Button();
            this.button41 = new System.Windows.Forms.Button();
            this.button73 = new System.Windows.Forms.Button();
            this.button72 = new System.Windows.Forms.Button();
            this.button71 = new System.Windows.Forms.Button();
            this.button70 = new System.Windows.Forms.Button();
            this.button68 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button54 = new System.Windows.Forms.Button();
            this.button69 = new System.Windows.Forms.Button();
            this.button34 = new System.Windows.Forms.Button();
            this.button63 = new System.Windows.Forms.Button();
            this.button67 = new System.Windows.Forms.Button();
            this.button61 = new System.Windows.Forms.Button();
            this.button52 = new System.Windows.Forms.Button();
            this.button53 = new System.Windows.Forms.Button();
            this.button60 = new System.Windows.Forms.Button();
            this.button58 = new System.Windows.Forms.Button();
            this.button59 = new System.Windows.Forms.Button();
            this.button51 = new System.Windows.Forms.Button();
            this.button42 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button56 = new System.Windows.Forms.Button();
            this.button57 = new System.Windows.Forms.Button();
            this.button62 = new System.Windows.Forms.Button();
            this.button48 = new System.Windows.Forms.Button();
            this.button37 = new System.Windows.Forms.Button();
            this.button55 = new System.Windows.Forms.Button();
            this.button44 = new System.Windows.Forms.Button();
            this.button50 = new System.Windows.Forms.Button();
            this.button43 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button49 = new System.Windows.Forms.Button();
            this.button40 = new System.Windows.Forms.Button();
            this.button39 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.button38 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button27 = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button47 = new System.Windows.Forms.Button();
            this.button46 = new System.Windows.Forms.Button();
            this.button45 = new System.Windows.Forms.Button();
            this.button36 = new System.Windows.Forms.Button();
            this.button29 = new System.Windows.Forms.Button();
            this.button26 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button81 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button91 = new System.Windows.Forms.Button();
            this.button90 = new System.Windows.Forms.Button();
            this.button87 = new System.Windows.Forms.Button();
            this.button88 = new System.Windows.Forms.Button();
            this.button89 = new System.Windows.Forms.Button();
            this.button86 = new System.Windows.Forms.Button();
            this.button85 = new System.Windows.Forms.Button();
            this.button84 = new System.Windows.Forms.Button();
            this.button83 = new System.Windows.Forms.Button();
            this.button92 = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button93 = new System.Windows.Forms.Button();
            this.button94 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.button82);
            this.panel1.Controls.Add(this.button78);
            this.panel1.Controls.Add(this.button35);
            this.panel1.Controls.Add(this.button33);
            this.panel1.Controls.Add(this.button32);
            this.panel1.Controls.Add(this.button76);
            this.panel1.Controls.Add(this.button80);
            this.panel1.Controls.Add(this.button65);
            this.panel1.Controls.Add(this.button79);
            this.panel1.Controls.Add(this.button75);
            this.panel1.Controls.Add(this.button77);
            this.panel1.Controls.Add(this.button74);
            this.panel1.Controls.Add(this.button66);
            this.panel1.Controls.Add(this.button21);
            this.panel1.Controls.Add(this.button20);
            this.panel1.Controls.Add(this.button64);
            this.panel1.Controls.Add(this.button41);
            this.panel1.Controls.Add(this.button73);
            this.panel1.Controls.Add(this.button72);
            this.panel1.Controls.Add(this.button71);
            this.panel1.Controls.Add(this.button70);
            this.panel1.Controls.Add(this.button68);
            this.panel1.Controls.Add(this.button23);
            this.panel1.Controls.Add(this.button54);
            this.panel1.Controls.Add(this.button69);
            this.panel1.Controls.Add(this.button34);
            this.panel1.Controls.Add(this.button63);
            this.panel1.Controls.Add(this.button67);
            this.panel1.Controls.Add(this.button61);
            this.panel1.Controls.Add(this.button52);
            this.panel1.Controls.Add(this.button53);
            this.panel1.Controls.Add(this.button60);
            this.panel1.Controls.Add(this.button58);
            this.panel1.Controls.Add(this.button59);
            this.panel1.Controls.Add(this.button51);
            this.panel1.Controls.Add(this.button42);
            this.panel1.Controls.Add(this.button19);
            this.panel1.Controls.Add(this.button56);
            this.panel1.Controls.Add(this.button57);
            this.panel1.Controls.Add(this.button62);
            this.panel1.Controls.Add(this.button48);
            this.panel1.Controls.Add(this.button37);
            this.panel1.Controls.Add(this.button55);
            this.panel1.Controls.Add(this.button44);
            this.panel1.Controls.Add(this.button50);
            this.panel1.Controls.Add(this.button43);
            this.panel1.Controls.Add(this.button22);
            this.panel1.Controls.Add(this.button49);
            this.panel1.Controls.Add(this.button40);
            this.panel1.Controls.Add(this.button39);
            this.panel1.Controls.Add(this.button31);
            this.panel1.Controls.Add(this.button38);
            this.panel1.Controls.Add(this.button18);
            this.panel1.Controls.Add(this.button27);
            this.panel1.Controls.Add(this.button28);
            this.panel1.Controls.Add(this.button30);
            this.panel1.Controls.Add(this.button25);
            this.panel1.Controls.Add(this.button11);
            this.panel1.Controls.Add(this.button16);
            this.panel1.Controls.Add(this.button15);
            this.panel1.Controls.Add(this.button17);
            this.panel1.Controls.Add(this.button10);
            this.panel1.Controls.Add(this.button47);
            this.panel1.Controls.Add(this.button46);
            this.panel1.Controls.Add(this.button45);
            this.panel1.Controls.Add(this.button36);
            this.panel1.Controls.Add(this.button29);
            this.panel1.Controls.Add(this.button26);
            this.panel1.Controls.Add(this.button24);
            this.panel1.Controls.Add(this.button13);
            this.panel1.Controls.Add(this.button14);
            this.panel1.Controls.Add(this.button12);
            this.panel1.Controls.Add(this.button8);
            this.panel1.Controls.Add(this.button9);
            this.panel1.Controls.Add(this.button5);
            this.panel1.Controls.Add(this.button6);
            this.panel1.Controls.Add(this.button7);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Location = new System.Drawing.Point(50, 25);
            this.panel1.Margin = new System.Windows.Forms.Padding(1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(470, 478);
            this.panel1.TabIndex = 3;
            // 
            // button82
            // 
            this.button82.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button82.Location = new System.Drawing.Point(419, 427);
            this.button82.Margin = new System.Windows.Forms.Padding(1);
            this.button82.Name = "button82";
            this.button82.Size = new System.Drawing.Size(50, 50);
            this.button82.TabIndex = 80;
            this.button82.Tag = "81";
            this.button82.UseVisualStyleBackColor = false;
            this.button82.Click += new System.EventHandler(this.button82_Click);
            // 
            // button78
            // 
            this.button78.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button78.Location = new System.Drawing.Point(263, 427);
            this.button78.Margin = new System.Windows.Forms.Padding(1);
            this.button78.Name = "button78";
            this.button78.Size = new System.Drawing.Size(50, 50);
            this.button78.TabIndex = 77;
            this.button78.Tag = "78";
            this.button78.UseVisualStyleBackColor = false;
            this.button78.Click += new System.EventHandler(this.button78_Click);
            // 
            // button35
            // 
            this.button35.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button35.Location = new System.Drawing.Point(367, 161);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(50, 50);
            this.button35.TabIndex = 34;
            this.button35.Tag = "35";
            this.button35.UseVisualStyleBackColor = false;
            this.button35.Click += new System.EventHandler(this.button35_Click);
            // 
            // button33
            // 
            this.button33.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button33.Location = new System.Drawing.Point(263, 161);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(50, 50);
            this.button33.TabIndex = 32;
            this.button33.Tag = "33";
            this.button33.UseVisualStyleBackColor = false;
            this.button33.Click += new System.EventHandler(this.button33_Click);
            // 
            // button32
            // 
            this.button32.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button32.Location = new System.Drawing.Point(211, 161);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(50, 50);
            this.button32.TabIndex = 31;
            this.button32.Tag = "32";
            this.button32.UseVisualStyleBackColor = false;
            this.button32.Click += new System.EventHandler(this.button32_Click);
            // 
            // button76
            // 
            this.button76.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button76.Location = new System.Drawing.Point(159, 427);
            this.button76.Margin = new System.Windows.Forms.Padding(1);
            this.button76.Name = "button76";
            this.button76.Size = new System.Drawing.Size(50, 50);
            this.button76.TabIndex = 75;
            this.button76.Tag = "76";
            this.button76.UseVisualStyleBackColor = false;
            this.button76.Click += new System.EventHandler(this.button76_Click);
            // 
            // button80
            // 
            this.button80.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button80.Location = new System.Drawing.Point(367, 427);
            this.button80.Margin = new System.Windows.Forms.Padding(1);
            this.button80.Name = "button80";
            this.button80.Size = new System.Drawing.Size(50, 50);
            this.button80.TabIndex = 79;
            this.button80.Tag = "80";
            this.button80.UseVisualStyleBackColor = false;
            this.button80.Click += new System.EventHandler(this.button80_Click);
            // 
            // button65
            // 
            this.button65.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button65.Location = new System.Drawing.Point(53, 375);
            this.button65.Name = "button65";
            this.button65.Size = new System.Drawing.Size(50, 50);
            this.button65.TabIndex = 64;
            this.button65.Tag = "65";
            this.button65.UseVisualStyleBackColor = false;
            this.button65.Click += new System.EventHandler(this.button65_Click);
            // 
            // button79
            // 
            this.button79.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button79.Location = new System.Drawing.Point(315, 427);
            this.button79.Margin = new System.Windows.Forms.Padding(1);
            this.button79.Name = "button79";
            this.button79.Size = new System.Drawing.Size(50, 50);
            this.button79.TabIndex = 78;
            this.button79.Tag = "79";
            this.button79.UseVisualStyleBackColor = false;
            this.button79.Click += new System.EventHandler(this.button79_Click);
            // 
            // button75
            // 
            this.button75.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button75.Location = new System.Drawing.Point(105, 427);
            this.button75.Name = "button75";
            this.button75.Size = new System.Drawing.Size(50, 50);
            this.button75.TabIndex = 74;
            this.button75.Tag = "75";
            this.button75.UseVisualStyleBackColor = false;
            this.button75.Click += new System.EventHandler(this.button75_Click);
            // 
            // button77
            // 
            this.button77.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button77.Location = new System.Drawing.Point(211, 427);
            this.button77.Margin = new System.Windows.Forms.Padding(1);
            this.button77.Name = "button77";
            this.button77.Size = new System.Drawing.Size(50, 50);
            this.button77.TabIndex = 76;
            this.button77.Tag = "77";
            this.button77.UseVisualStyleBackColor = false;
            this.button77.Click += new System.EventHandler(this.button77_Click);
            // 
            // button74
            // 
            this.button74.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button74.Location = new System.Drawing.Point(53, 427);
            this.button74.Name = "button74";
            this.button74.Size = new System.Drawing.Size(50, 50);
            this.button74.TabIndex = 73;
            this.button74.Tag = "74";
            this.button74.UseVisualStyleBackColor = false;
            this.button74.Click += new System.EventHandler(this.button74_Click);
            // 
            // button66
            // 
            this.button66.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button66.Location = new System.Drawing.Point(105, 375);
            this.button66.Name = "button66";
            this.button66.Size = new System.Drawing.Size(50, 50);
            this.button66.TabIndex = 65;
            this.button66.Tag = "66";
            this.button66.UseVisualStyleBackColor = false;
            this.button66.Click += new System.EventHandler(this.button66_Click);
            // 
            // button21
            // 
            this.button21.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button21.Location = new System.Drawing.Point(105, 109);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(50, 50);
            this.button21.TabIndex = 20;
            this.button21.Tag = "21";
            this.button21.UseVisualStyleBackColor = false;
            this.button21.Click += new System.EventHandler(this.button21_Click);
            // 
            // button20
            // 
            this.button20.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button20.Location = new System.Drawing.Point(53, 109);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(50, 50);
            this.button20.TabIndex = 19;
            this.button20.Tag = "20";
            this.button20.UseVisualStyleBackColor = false;
            this.button20.Click += new System.EventHandler(this.button20_Click);
            // 
            // button64
            // 
            this.button64.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button64.Location = new System.Drawing.Point(-1, 375);
            this.button64.Name = "button64";
            this.button64.Size = new System.Drawing.Size(50, 50);
            this.button64.TabIndex = 63;
            this.button64.Tag = "64";
            this.button64.UseVisualStyleBackColor = false;
            this.button64.Click += new System.EventHandler(this.button64_Click);
            // 
            // button41
            // 
            this.button41.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button41.Location = new System.Drawing.Point(211, 215);
            this.button41.Name = "button41";
            this.button41.Size = new System.Drawing.Size(50, 50);
            this.button41.TabIndex = 40;
            this.button41.Tag = "41";
            this.button41.UseVisualStyleBackColor = false;
            this.button41.Click += new System.EventHandler(this.button41_Click);
            // 
            // button73
            // 
            this.button73.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button73.Location = new System.Drawing.Point(-1, 427);
            this.button73.Margin = new System.Windows.Forms.Padding(1);
            this.button73.Name = "button73";
            this.button73.Size = new System.Drawing.Size(50, 50);
            this.button73.TabIndex = 72;
            this.button73.Tag = "73";
            this.button73.UseVisualStyleBackColor = false;
            this.button73.Click += new System.EventHandler(this.button73_Click);
            // 
            // button72
            // 
            this.button72.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button72.Location = new System.Drawing.Point(419, 375);
            this.button72.Name = "button72";
            this.button72.Size = new System.Drawing.Size(50, 50);
            this.button72.TabIndex = 71;
            this.button72.Tag = "72";
            this.button72.UseVisualStyleBackColor = false;
            this.button72.Click += new System.EventHandler(this.button72_Click);
            // 
            // button71
            // 
            this.button71.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button71.Location = new System.Drawing.Point(367, 375);
            this.button71.Name = "button71";
            this.button71.Size = new System.Drawing.Size(50, 50);
            this.button71.TabIndex = 70;
            this.button71.Tag = "71";
            this.button71.UseVisualStyleBackColor = false;
            this.button71.Click += new System.EventHandler(this.button71_Click);
            // 
            // button70
            // 
            this.button70.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button70.Location = new System.Drawing.Point(315, 375);
            this.button70.Name = "button70";
            this.button70.Size = new System.Drawing.Size(50, 50);
            this.button70.TabIndex = 69;
            this.button70.Tag = "70";
            this.button70.UseVisualStyleBackColor = false;
            this.button70.Click += new System.EventHandler(this.button70_Click);
            // 
            // button68
            // 
            this.button68.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button68.Location = new System.Drawing.Point(211, 375);
            this.button68.Name = "button68";
            this.button68.Size = new System.Drawing.Size(50, 50);
            this.button68.TabIndex = 67;
            this.button68.Tag = "68";
            this.button68.UseVisualStyleBackColor = false;
            this.button68.Click += new System.EventHandler(this.button68_Click);
            // 
            // button23
            // 
            this.button23.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button23.Location = new System.Drawing.Point(211, 108);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(50, 50);
            this.button23.TabIndex = 22;
            this.button23.Tag = "23";
            this.button23.UseVisualStyleBackColor = false;
            this.button23.Click += new System.EventHandler(this.button23_Click);
            // 
            // button54
            // 
            this.button54.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button54.Location = new System.Drawing.Point(419, 269);
            this.button54.Name = "button54";
            this.button54.Size = new System.Drawing.Size(50, 50);
            this.button54.TabIndex = 53;
            this.button54.Tag = "54";
            this.button54.UseVisualStyleBackColor = false;
            this.button54.Click += new System.EventHandler(this.button54_Click);
            // 
            // button69
            // 
            this.button69.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button69.Location = new System.Drawing.Point(263, 375);
            this.button69.Name = "button69";
            this.button69.Size = new System.Drawing.Size(50, 50);
            this.button69.TabIndex = 68;
            this.button69.Tag = "69";
            this.button69.UseVisualStyleBackColor = false;
            this.button69.Click += new System.EventHandler(this.button69_Click);
            // 
            // button34
            // 
            this.button34.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button34.Location = new System.Drawing.Point(315, 161);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(50, 50);
            this.button34.TabIndex = 33;
            this.button34.Tag = "34";
            this.button34.UseVisualStyleBackColor = false;
            this.button34.Click += new System.EventHandler(this.button34_Click);
            // 
            // button63
            // 
            this.button63.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button63.Location = new System.Drawing.Point(419, 323);
            this.button63.Margin = new System.Windows.Forms.Padding(1);
            this.button63.Name = "button63";
            this.button63.Size = new System.Drawing.Size(50, 50);
            this.button63.TabIndex = 62;
            this.button63.Tag = "63";
            this.button63.UseVisualStyleBackColor = false;
            this.button63.Click += new System.EventHandler(this.button63_Click);
            // 
            // button67
            // 
            this.button67.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button67.Location = new System.Drawing.Point(159, 375);
            this.button67.Name = "button67";
            this.button67.Size = new System.Drawing.Size(50, 50);
            this.button67.TabIndex = 66;
            this.button67.Tag = "67";
            this.button67.UseVisualStyleBackColor = false;
            this.button67.Click += new System.EventHandler(this.button67_Click);
            // 
            // button61
            // 
            this.button61.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button61.Location = new System.Drawing.Point(315, 323);
            this.button61.Margin = new System.Windows.Forms.Padding(1);
            this.button61.Name = "button61";
            this.button61.Size = new System.Drawing.Size(50, 50);
            this.button61.TabIndex = 60;
            this.button61.Tag = "61";
            this.button61.UseVisualStyleBackColor = false;
            this.button61.Click += new System.EventHandler(this.button61_Click);
            // 
            // button52
            // 
            this.button52.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button52.Location = new System.Drawing.Point(315, 269);
            this.button52.Name = "button52";
            this.button52.Size = new System.Drawing.Size(50, 50);
            this.button52.TabIndex = 51;
            this.button52.Tag = "52";
            this.button52.UseVisualStyleBackColor = false;
            this.button52.Click += new System.EventHandler(this.button52_Click);
            // 
            // button53
            // 
            this.button53.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button53.Location = new System.Drawing.Point(367, 269);
            this.button53.Margin = new System.Windows.Forms.Padding(1);
            this.button53.Name = "button53";
            this.button53.Size = new System.Drawing.Size(50, 50);
            this.button53.TabIndex = 52;
            this.button53.Tag = "53";
            this.button53.UseVisualStyleBackColor = false;
            this.button53.Click += new System.EventHandler(this.button53_Click);
            // 
            // button60
            // 
            this.button60.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button60.Location = new System.Drawing.Point(263, 323);
            this.button60.Margin = new System.Windows.Forms.Padding(1);
            this.button60.Name = "button60";
            this.button60.Size = new System.Drawing.Size(50, 50);
            this.button60.TabIndex = 59;
            this.button60.Tag = "60";
            this.button60.UseVisualStyleBackColor = false;
            this.button60.Click += new System.EventHandler(this.button60_Click);
            // 
            // button58
            // 
            this.button58.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button58.Location = new System.Drawing.Point(159, 323);
            this.button58.Name = "button58";
            this.button58.Size = new System.Drawing.Size(50, 50);
            this.button58.TabIndex = 57;
            this.button58.Tag = "58";
            this.button58.UseVisualStyleBackColor = false;
            this.button58.Click += new System.EventHandler(this.button58_Click);
            // 
            // button59
            // 
            this.button59.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button59.Location = new System.Drawing.Point(211, 323);
            this.button59.Margin = new System.Windows.Forms.Padding(1);
            this.button59.Name = "button59";
            this.button59.Size = new System.Drawing.Size(50, 50);
            this.button59.TabIndex = 58;
            this.button59.Tag = "59";
            this.button59.UseVisualStyleBackColor = false;
            this.button59.Click += new System.EventHandler(this.button59_Click);
            // 
            // button51
            // 
            this.button51.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button51.Location = new System.Drawing.Point(263, 269);
            this.button51.Name = "button51";
            this.button51.Size = new System.Drawing.Size(50, 50);
            this.button51.TabIndex = 50;
            this.button51.Tag = "51";
            this.button51.UseVisualStyleBackColor = false;
            this.button51.Click += new System.EventHandler(this.button51_Click);
            // 
            // button42
            // 
            this.button42.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button42.Location = new System.Drawing.Point(263, 215);
            this.button42.Margin = new System.Windows.Forms.Padding(1);
            this.button42.Name = "button42";
            this.button42.Size = new System.Drawing.Size(50, 50);
            this.button42.TabIndex = 41;
            this.button42.Tag = "42";
            this.button42.UseVisualStyleBackColor = false;
            this.button42.Click += new System.EventHandler(this.button42_Click);
            // 
            // button19
            // 
            this.button19.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button19.Location = new System.Drawing.Point(-1, 108);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(50, 50);
            this.button19.TabIndex = 18;
            this.button19.Tag = "19";
            this.button19.UseVisualStyleBackColor = false;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // button56
            // 
            this.button56.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button56.Location = new System.Drawing.Point(53, 323);
            this.button56.Name = "button56";
            this.button56.Size = new System.Drawing.Size(50, 50);
            this.button56.TabIndex = 55;
            this.button56.Tag = "56";
            this.button56.UseVisualStyleBackColor = false;
            this.button56.Click += new System.EventHandler(this.button56_Click);
            // 
            // button57
            // 
            this.button57.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button57.Location = new System.Drawing.Point(105, 323);
            this.button57.Name = "button57";
            this.button57.Size = new System.Drawing.Size(50, 50);
            this.button57.TabIndex = 56;
            this.button57.Tag = "57";
            this.button57.UseVisualStyleBackColor = false;
            this.button57.Click += new System.EventHandler(this.button57_Click);
            // 
            // button62
            // 
            this.button62.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button62.Location = new System.Drawing.Point(367, 323);
            this.button62.Margin = new System.Windows.Forms.Padding(1);
            this.button62.Name = "button62";
            this.button62.Size = new System.Drawing.Size(50, 50);
            this.button62.TabIndex = 61;
            this.button62.Tag = "62";
            this.button62.UseVisualStyleBackColor = false;
            this.button62.Click += new System.EventHandler(this.button62_Click);
            // 
            // button48
            // 
            this.button48.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button48.Location = new System.Drawing.Point(105, 269);
            this.button48.Margin = new System.Windows.Forms.Padding(1);
            this.button48.Name = "button48";
            this.button48.Size = new System.Drawing.Size(50, 50);
            this.button48.TabIndex = 47;
            this.button48.Tag = "48";
            this.button48.UseVisualStyleBackColor = false;
            this.button48.Click += new System.EventHandler(this.button48_Click);
            // 
            // button37
            // 
            this.button37.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button37.Location = new System.Drawing.Point(-1, 215);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(50, 50);
            this.button37.TabIndex = 36;
            this.button37.Tag = "37";
            this.button37.UseVisualStyleBackColor = false;
            this.button37.Click += new System.EventHandler(this.button37_Click);
            // 
            // button55
            // 
            this.button55.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button55.Location = new System.Drawing.Point(-1, 323);
            this.button55.Name = "button55";
            this.button55.Size = new System.Drawing.Size(50, 50);
            this.button55.TabIndex = 54;
            this.button55.Tag = "55";
            this.button55.UseVisualStyleBackColor = false;
            this.button55.Click += new System.EventHandler(this.button55_Click);
            // 
            // button44
            // 
            this.button44.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button44.Location = new System.Drawing.Point(367, 215);
            this.button44.Margin = new System.Windows.Forms.Padding(1);
            this.button44.Name = "button44";
            this.button44.Size = new System.Drawing.Size(50, 50);
            this.button44.TabIndex = 43;
            this.button44.Tag = "44";
            this.button44.UseVisualStyleBackColor = false;
            this.button44.Click += new System.EventHandler(this.button44_Click);
            // 
            // button50
            // 
            this.button50.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button50.Location = new System.Drawing.Point(211, 269);
            this.button50.Name = "button50";
            this.button50.Size = new System.Drawing.Size(50, 50);
            this.button50.TabIndex = 49;
            this.button50.Tag = "50";
            this.button50.UseVisualStyleBackColor = false;
            this.button50.Click += new System.EventHandler(this.button50_Click);
            // 
            // button43
            // 
            this.button43.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button43.Location = new System.Drawing.Point(315, 215);
            this.button43.Margin = new System.Windows.Forms.Padding(1);
            this.button43.Name = "button43";
            this.button43.Size = new System.Drawing.Size(50, 50);
            this.button43.TabIndex = 42;
            this.button43.Tag = "42";
            this.button43.UseVisualStyleBackColor = false;
            this.button43.Click += new System.EventHandler(this.button43_Click);
            // 
            // button22
            // 
            this.button22.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button22.Location = new System.Drawing.Point(159, 108);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(50, 50);
            this.button22.TabIndex = 21;
            this.button22.Tag = "22";
            this.button22.UseVisualStyleBackColor = false;
            this.button22.Click += new System.EventHandler(this.button22_Click);
            // 
            // button49
            // 
            this.button49.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button49.Location = new System.Drawing.Point(159, 269);
            this.button49.Name = "button49";
            this.button49.Size = new System.Drawing.Size(50, 50);
            this.button49.TabIndex = 48;
            this.button49.Tag = "49";
            this.button49.UseVisualStyleBackColor = false;
            this.button49.Click += new System.EventHandler(this.button49_Click);
            // 
            // button40
            // 
            this.button40.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button40.Location = new System.Drawing.Point(159, 215);
            this.button40.Name = "button40";
            this.button40.Size = new System.Drawing.Size(50, 50);
            this.button40.TabIndex = 39;
            this.button40.Tag = "40";
            this.button40.UseVisualStyleBackColor = false;
            this.button40.Click += new System.EventHandler(this.button40_Click);
            // 
            // button39
            // 
            this.button39.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button39.Location = new System.Drawing.Point(105, 215);
            this.button39.Name = "button39";
            this.button39.Size = new System.Drawing.Size(50, 50);
            this.button39.TabIndex = 38;
            this.button39.Tag = "39";
            this.button39.UseVisualStyleBackColor = false;
            this.button39.Click += new System.EventHandler(this.button39_Click);
            // 
            // button31
            // 
            this.button31.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button31.Location = new System.Drawing.Point(159, 161);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(50, 50);
            this.button31.TabIndex = 30;
            this.button31.Tag = "31";
            this.button31.UseVisualStyleBackColor = false;
            this.button31.Click += new System.EventHandler(this.button31_Click);
            // 
            // button38
            // 
            this.button38.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button38.Location = new System.Drawing.Point(53, 215);
            this.button38.Name = "button38";
            this.button38.Size = new System.Drawing.Size(50, 50);
            this.button38.TabIndex = 37;
            this.button38.Tag = "38";
            this.button38.UseVisualStyleBackColor = false;
            this.button38.Click += new System.EventHandler(this.button38_Click);
            // 
            // button18
            // 
            this.button18.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button18.Location = new System.Drawing.Point(419, 53);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(50, 50);
            this.button18.TabIndex = 17;
            this.button18.Tag = "18";
            this.button18.UseVisualStyleBackColor = false;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // button27
            // 
            this.button27.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button27.Location = new System.Drawing.Point(419, 107);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(50, 50);
            this.button27.TabIndex = 26;
            this.button27.Tag = "27";
            this.button27.UseVisualStyleBackColor = false;
            this.button27.Click += new System.EventHandler(this.button27_Click);
            // 
            // button28
            // 
            this.button28.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button28.Location = new System.Drawing.Point(-1, 161);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(50, 50);
            this.button28.TabIndex = 27;
            this.button28.Tag = "28";
            this.button28.UseVisualStyleBackColor = false;
            this.button28.Click += new System.EventHandler(this.button28_Click);
            // 
            // button30
            // 
            this.button30.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button30.Location = new System.Drawing.Point(105, 161);
            this.button30.Margin = new System.Windows.Forms.Padding(1);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(50, 50);
            this.button30.TabIndex = 29;
            this.button30.Tag = "30";
            this.button30.UseVisualStyleBackColor = false;
            this.button30.Click += new System.EventHandler(this.button30_Click);
            // 
            // button25
            // 
            this.button25.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button25.Location = new System.Drawing.Point(315, 107);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(50, 50);
            this.button25.TabIndex = 24;
            this.button25.Tag = "25";
            this.button25.UseVisualStyleBackColor = false;
            this.button25.Click += new System.EventHandler(this.button25_Click);
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button11.Location = new System.Drawing.Point(53, 53);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(50, 50);
            this.button11.TabIndex = 10;
            this.button11.Tag = "11";
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button16
            // 
            this.button16.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button16.Location = new System.Drawing.Point(315, 53);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(50, 50);
            this.button16.TabIndex = 15;
            this.button16.Tag = "16";
            this.button16.UseVisualStyleBackColor = false;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button15.Location = new System.Drawing.Point(263, 53);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(50, 50);
            this.button15.TabIndex = 14;
            this.button15.Tag = "15";
            this.button15.UseVisualStyleBackColor = false;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // button17
            // 
            this.button17.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button17.Location = new System.Drawing.Point(367, 53);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(50, 50);
            this.button17.TabIndex = 16;
            this.button17.Tag = "17";
            this.button17.UseVisualStyleBackColor = false;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button10.Location = new System.Drawing.Point(-1, 53);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(50, 50);
            this.button10.TabIndex = 9;
            this.button10.Tag = "10";
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button47
            // 
            this.button47.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button47.Location = new System.Drawing.Point(53, 269);
            this.button47.Margin = new System.Windows.Forms.Padding(1);
            this.button47.Name = "button47";
            this.button47.Size = new System.Drawing.Size(50, 50);
            this.button47.TabIndex = 46;
            this.button47.Tag = "47";
            this.button47.UseVisualStyleBackColor = false;
            this.button47.Click += new System.EventHandler(this.button47_Click);
            // 
            // button46
            // 
            this.button46.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button46.Location = new System.Drawing.Point(-1, 269);
            this.button46.Margin = new System.Windows.Forms.Padding(1);
            this.button46.Name = "button46";
            this.button46.Size = new System.Drawing.Size(50, 50);
            this.button46.TabIndex = 45;
            this.button46.Tag = "46";
            this.button46.UseVisualStyleBackColor = false;
            this.button46.Click += new System.EventHandler(this.button46_Click);
            // 
            // button45
            // 
            this.button45.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button45.Location = new System.Drawing.Point(419, 215);
            this.button45.Margin = new System.Windows.Forms.Padding(1);
            this.button45.Name = "button45";
            this.button45.Size = new System.Drawing.Size(50, 50);
            this.button45.TabIndex = 44;
            this.button45.Tag = "45";
            this.button45.UseVisualStyleBackColor = false;
            this.button45.Click += new System.EventHandler(this.button45_Click);
            // 
            // button36
            // 
            this.button36.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button36.Location = new System.Drawing.Point(419, 161);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(50, 50);
            this.button36.TabIndex = 35;
            this.button36.Tag = "36";
            this.button36.UseVisualStyleBackColor = false;
            this.button36.Click += new System.EventHandler(this.button36_Click);
            // 
            // button29
            // 
            this.button29.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button29.Location = new System.Drawing.Point(53, 161);
            this.button29.Margin = new System.Windows.Forms.Padding(1);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(50, 50);
            this.button29.TabIndex = 28;
            this.button29.Tag = "29";
            this.button29.UseVisualStyleBackColor = false;
            this.button29.Click += new System.EventHandler(this.button29_Click);
            // 
            // button26
            // 
            this.button26.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button26.Location = new System.Drawing.Point(367, 107);
            this.button26.Margin = new System.Windows.Forms.Padding(1);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(50, 50);
            this.button26.TabIndex = 25;
            this.button26.Tag = "26";
            this.button26.UseVisualStyleBackColor = false;
            this.button26.Click += new System.EventHandler(this.button26_Click);
            // 
            // button24
            // 
            this.button24.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button24.Location = new System.Drawing.Point(263, 107);
            this.button24.Margin = new System.Windows.Forms.Padding(1);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(50, 50);
            this.button24.TabIndex = 23;
            this.button24.Tag = "24";
            this.button24.UseVisualStyleBackColor = false;
            this.button24.Click += new System.EventHandler(this.button24_Click);
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button13.Location = new System.Drawing.Point(159, 53);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(50, 50);
            this.button13.TabIndex = 12;
            this.button13.Tag = "13";
            this.button13.UseVisualStyleBackColor = false;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button14.Location = new System.Drawing.Point(211, 53);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(50, 50);
            this.button14.TabIndex = 13;
            this.button14.Tag = "14";
            this.button14.UseVisualStyleBackColor = false;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button12.Location = new System.Drawing.Point(105, 53);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(50, 50);
            this.button12.TabIndex = 11;
            this.button12.Tag = "12";
            this.button12.UseVisualStyleBackColor = false;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button8.Location = new System.Drawing.Point(367, -1);
            this.button8.Margin = new System.Windows.Forms.Padding(1);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(50, 50);
            this.button8.TabIndex = 7;
            this.button8.Tag = "8";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button9.Location = new System.Drawing.Point(419, -1);
            this.button9.Margin = new System.Windows.Forms.Padding(1);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(50, 50);
            this.button9.TabIndex = 8;
            this.button9.Tag = "9";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button5.Location = new System.Drawing.Point(211, -1);
            this.button5.Margin = new System.Windows.Forms.Padding(1);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(50, 50);
            this.button5.TabIndex = 4;
            this.button5.Tag = "5";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button6.Location = new System.Drawing.Point(263, -1);
            this.button6.Margin = new System.Windows.Forms.Padding(1);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(50, 50);
            this.button6.TabIndex = 5;
            this.button6.Tag = "6";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button7.Location = new System.Drawing.Point(315, -1);
            this.button7.Margin = new System.Windows.Forms.Padding(1);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(50, 50);
            this.button7.TabIndex = 6;
            this.button7.Tag = "7";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button4.Location = new System.Drawing.Point(159, -1);
            this.button4.Margin = new System.Windows.Forms.Padding(1);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(50, 50);
            this.button4.TabIndex = 3;
            this.button4.Tag = "4";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button3.Location = new System.Drawing.Point(105, -1);
            this.button3.Margin = new System.Windows.Forms.Padding(1);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(50, 50);
            this.button3.TabIndex = 2;
            this.button3.Tag = "3";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button2.ForeColor = System.Drawing.Color.Black;
            this.button2.Location = new System.Drawing.Point(53, -1);
            this.button2.Margin = new System.Windows.Forms.Padding(1);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(50, 50);
            this.button2.TabIndex = 1;
            this.button2.Tag = "2";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button1.Location = new System.Drawing.Point(-1, -1);
            this.button1.Margin = new System.Windows.Forms.Padding(1);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(50, 50);
            this.button1.TabIndex = 0;
            this.button1.Tag = "1";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button81
            // 
            this.button81.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button81.Location = new System.Drawing.Point(635, 216);
            this.button81.Name = "button81";
            this.button81.Size = new System.Drawing.Size(164, 45);
            this.button81.TabIndex = 4;
            this.button81.Text = "Nowa Gra";
            this.button81.UseVisualStyleBackColor = true;
            this.button81.Click += new System.EventHandler(this.button81_Click);
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(732, 32);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(164, 30);
            this.textBox1.TabIndex = 5;
            this.textBox1.Text = "Time: 00:00:00";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button91);
            this.panel2.Controls.Add(this.button90);
            this.panel2.Controls.Add(this.button87);
            this.panel2.Controls.Add(this.button88);
            this.panel2.Controls.Add(this.button89);
            this.panel2.Controls.Add(this.button86);
            this.panel2.Controls.Add(this.button85);
            this.panel2.Controls.Add(this.button84);
            this.panel2.Controls.Add(this.button83);
            this.panel2.Location = new System.Drawing.Point(526, 25);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(182, 79);
            this.panel2.TabIndex = 6;
            this.panel2.Visible = false;
            // 
            // button91
            // 
            this.button91.Location = new System.Drawing.Point(111, 39);
            this.button91.Name = "button91";
            this.button91.Size = new System.Drawing.Size(30, 30);
            this.button91.TabIndex = 11;
            this.button91.Text = "9";
            this.button91.UseVisualStyleBackColor = true;
            this.button91.Click += new System.EventHandler(this.button91_Click);
            // 
            // button90
            // 
            this.button90.Location = new System.Drawing.Point(75, 39);
            this.button90.Name = "button90";
            this.button90.Size = new System.Drawing.Size(30, 30);
            this.button90.TabIndex = 10;
            this.button90.Text = "8";
            this.button90.UseVisualStyleBackColor = true;
            this.button90.Click += new System.EventHandler(this.button90_Click);
            // 
            // button87
            // 
            this.button87.Location = new System.Drawing.Point(39, 39);
            this.button87.Name = "button87";
            this.button87.Size = new System.Drawing.Size(30, 30);
            this.button87.TabIndex = 7;
            this.button87.Text = "7";
            this.button87.UseVisualStyleBackColor = true;
            this.button87.Click += new System.EventHandler(this.button87_Click);
            // 
            // button88
            // 
            this.button88.Location = new System.Drawing.Point(3, 39);
            this.button88.Name = "button88";
            this.button88.Size = new System.Drawing.Size(30, 30);
            this.button88.TabIndex = 8;
            this.button88.Text = "6";
            this.button88.UseVisualStyleBackColor = true;
            this.button88.Click += new System.EventHandler(this.button88_Click);
            // 
            // button89
            // 
            this.button89.Location = new System.Drawing.Point(147, 3);
            this.button89.Name = "button89";
            this.button89.Size = new System.Drawing.Size(30, 30);
            this.button89.TabIndex = 9;
            this.button89.Text = "5";
            this.button89.UseVisualStyleBackColor = true;
            this.button89.Click += new System.EventHandler(this.button89_Click);
            // 
            // button86
            // 
            this.button86.Location = new System.Drawing.Point(39, 3);
            this.button86.Name = "button86";
            this.button86.Size = new System.Drawing.Size(30, 30);
            this.button86.TabIndex = 3;
            this.button86.Text = "2";
            this.button86.UseVisualStyleBackColor = true;
            this.button86.Click += new System.EventHandler(this.button86_Click);
            // 
            // button85
            // 
            this.button85.Location = new System.Drawing.Point(75, 3);
            this.button85.Name = "button85";
            this.button85.Size = new System.Drawing.Size(30, 30);
            this.button85.TabIndex = 2;
            this.button85.Text = "3";
            this.button85.UseVisualStyleBackColor = true;
            this.button85.Click += new System.EventHandler(this.button85_Click);
            // 
            // button84
            // 
            this.button84.Location = new System.Drawing.Point(111, 3);
            this.button84.Name = "button84";
            this.button84.Size = new System.Drawing.Size(30, 30);
            this.button84.TabIndex = 1;
            this.button84.Text = "4";
            this.button84.UseVisualStyleBackColor = true;
            this.button84.Click += new System.EventHandler(this.button84_Click);
            // 
            // button83
            // 
            this.button83.Location = new System.Drawing.Point(3, 3);
            this.button83.Name = "button83";
            this.button83.Size = new System.Drawing.Size(30, 30);
            this.button83.TabIndex = 0;
            this.button83.Text = "1";
            this.button83.UseVisualStyleBackColor = true;
            this.button83.Click += new System.EventHandler(this.button83_Click);
            // 
            // button92
            // 
            this.button92.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button92.Location = new System.Drawing.Point(635, 267);
            this.button92.Name = "button92";
            this.button92.Size = new System.Drawing.Size(164, 45);
            this.button92.TabIndex = 7;
            this.button92.Text = "Podpowiedź";
            this.button92.UseVisualStyleBackColor = true;
            this.button92.Click += new System.EventHandler(this.button92_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "łatwy",
            "średni",
            "trudny"});
            this.comboBox1.Location = new System.Drawing.Point(732, 97);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(167, 28);
            this.comboBox1.TabIndex = 8;
            this.comboBox1.Text = "łatwy";
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(728, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(171, 25);
            this.label1.TabIndex = 9;
            this.label1.Text = "Poziom trudności";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button93
            // 
            this.button93.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button93.Location = new System.Drawing.Point(637, 318);
            this.button93.Name = "button93";
            this.button93.Size = new System.Drawing.Size(164, 45);
            this.button93.TabIndex = 10;
            this.button93.Text = "Powrót";
            this.button93.UseVisualStyleBackColor = true;
            this.button93.Click += new System.EventHandler(this.button93_Click);
            // 
            // button94
            // 
            this.button94.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button94.Location = new System.Drawing.Point(637, 369);
            this.button94.Name = "button94";
            this.button94.Size = new System.Drawing.Size(162, 45);
            this.button94.TabIndex = 11;
            this.button94.Text = "Zapisz";
            this.button94.UseVisualStyleBackColor = true;
            this.button94.Click += new System.EventHandler(this.button94_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(930, 566);
            this.Controls.Add(this.button94);
            this.Controls.Add(this.button93);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.button92);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button81);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Sudoku";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Rysuj);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button53;
        private System.Windows.Forms.Button button48;
        private System.Windows.Forms.Button button47;
        private System.Windows.Forms.Button button46;
        private System.Windows.Forms.Button button45;
        private System.Windows.Forms.Button button44;
        private System.Windows.Forms.Button button43;
        private System.Windows.Forms.Button button42;
        private System.Windows.Forms.Button button41;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.Button button40;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.Button button38;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.Button button35;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button49;
        private System.Windows.Forms.Button button50;
        private System.Windows.Forms.Button button51;
        private System.Windows.Forms.Button button52;
        private System.Windows.Forms.Button button80;
        private System.Windows.Forms.Button button79;
        private System.Windows.Forms.Button button78;
        private System.Windows.Forms.Button button77;
        private System.Windows.Forms.Button button76;
        private System.Windows.Forms.Button button75;
        private System.Windows.Forms.Button button74;
        private System.Windows.Forms.Button button73;
        private System.Windows.Forms.Button button64;
        private System.Windows.Forms.Button button65;
        private System.Windows.Forms.Button button66;
        private System.Windows.Forms.Button button67;
        private System.Windows.Forms.Button button68;
        private System.Windows.Forms.Button button69;
        private System.Windows.Forms.Button button70;
        private System.Windows.Forms.Button button71;
        private System.Windows.Forms.Button button72;
        private System.Windows.Forms.Button button63;
        private System.Windows.Forms.Button button62;
        private System.Windows.Forms.Button button61;
        private System.Windows.Forms.Button button60;
        private System.Windows.Forms.Button button59;
        private System.Windows.Forms.Button button54;
        private System.Windows.Forms.Button button55;
        private System.Windows.Forms.Button button56;
        private System.Windows.Forms.Button button57;
        private System.Windows.Forms.Button button58;
        private System.Windows.Forms.Button button81;
        private System.Windows.Forms.Button button82;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button91;
        private System.Windows.Forms.Button button90;
        private System.Windows.Forms.Button button87;
        private System.Windows.Forms.Button button88;
        private System.Windows.Forms.Button button89;
        private System.Windows.Forms.Button button86;
        private System.Windows.Forms.Button button85;
        private System.Windows.Forms.Button button84;
        private System.Windows.Forms.Button button83;
        private System.Windows.Forms.Button button92;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button93;
        private System.Windows.Forms.Button button94;
    }
}

