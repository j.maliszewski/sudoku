﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sudoku
{
    public partial class Form2 : Form
    {
        public static string name;
        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form3 f3 = new Form3();
            f3.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.panel3.Visible = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button93_Click(object sender, EventArgs e)
        {
            if (textBox2.Text != "")
            {
                name = textBox2.Text;
                this.Hide();
                Form1 f1 = new Form1();
                f1.ShowDialog();
            }
            else
                MessageBox.Show("Musisz podać nazwę!", "Uwaga", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 f1 = new Form1();
            Form1.wczytywanie = true;
            f1.ShowDialog();
            
        }
    }
}
