﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;

namespace Sudoku
{

    public partial class Form1 : Form
    {
        System.Timers.Timer timer;
        int h, m, s;
        int[] kolory = new int[81];
        int ilosc = 0;
        int x, y;
        int[] pom;
        int[,] tablica = new int[rozmiar, rozmiar];
        int[,] temp = new int[rozmiar,rozmiar];
        int[] dostepne = new int[9] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        int poziom = 50;
        Button b;
        const int rozmiar = 9;
        bool podpowiedz = false;
        public static bool wczytywanie = false;
        public Form1()
        {
            InitializeComponent();
        }
        private void Rysuj(object sender, PaintEventArgs e)
        {
            Graphics go = panel1.CreateGraphics();
            Brush red = new SolidBrush(Color.Red);
            Pen pen = new Pen(red, 1);
            go.DrawLine(pen, 117, 0, 117, 400);
            go.DrawLine(pen, 235, 0, 235, 400);
            go.DrawLine(pen, 0, 130, 450, 130);
            go.DrawLine(pen, 0, 260, 450, 260);
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            timer = new System.Timers.Timer();
            if (wczytywanie)
            {
                int? l;
                int i=0;
                int?[] odczyt = new int?[81];
                StreamReader we = new StreamReader("Zapis.txt");
                Form2.name = we.ReadLine();
                string line = we.ReadLine();
                string[] kolor = line.Split(',');
                string line2 = we.ReadLine();
                string[] wynik = line2.Split(',');
                h = int.Parse(we.ReadLine());
                m = int.Parse(we.ReadLine());
                s = int.Parse(we.ReadLine());
                textBox1.Text = "Time: " + string.Format("{0}:{1}:{2}", h.ToString().PadLeft(2, '0'), m.ToString().PadLeft(2, '0'), s.ToString().PadLeft(2, '0'));
                comboBox1.Text = we.ReadLine();
                int? a;
                while ((a = we.Read()) != null){
                    l = a - 48;
                    if(l < 10 && l >= 0)
                    {
                        odczyt[i] = l;
                        i++;
                        if (i == 81)
                            break;
                    }
                }
                pom = new int[82];
                for(int r = 0; r < kolor.Length-1; r++)
                {
                    kolory[int.Parse(kolor[r])] = 2;
                }
                for(int r = 0; r < wynik.Length-1; r++)
                {
                    pom[r] = int.Parse(wynik[r]);
                }
                foreach(Button b in this.panel1.Controls){
                    if (odczyt[b.TabIndex] == 0)
                        b.Text = "";
                    else
                        b.Text = odczyt[b.TabIndex].ToString();
                    if (kolory[b.TabIndex] == 2)
                        b.BackColor = Color.Yellow;
                }
                
                we.Close();
                timer.Start();
                timer.Interval = 1000;
                timer.Elapsed += OnTimeEvent;                 
            }
            else
            {
                timer = new System.Timers.Timer();
                timer.Interval = 1000;
                timer.Elapsed += OnTimeEvent;
            }
        }
        private void OnTimeEvent(object sender, ElapsedEventArgs e)
        {

            Invoke(new Action(() =>
            {
                s += 1;
                if (s == 60)
                {
                    s = 0;
                    m += 1;
                }
                if (m == 60)
                {
                    m = 0;
                    h += 1;
                }
                textBox1.Text = "Time: " + string.Format("{0}:{1}:{2}", h.ToString().PadLeft(2, '0'), m.ToString().PadLeft(2, '0'), s.ToString().PadLeft(2, '0'));
            }));
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            timer.Stop();
            Application.Exit();
        }

        private void button81_Click(object sender, EventArgs e)
        {
            zeruj(kolory);
            foreach(Control control in panel1.Controls)
            {
                timer.Stop();
                h = m = s = 0;
                textBox1.Text = "Time: " + string.Format("{0}:{1}:{2}", h.ToString().PadLeft(2, '0'), m.ToString().PadLeft(2, '0'), s.ToString().PadLeft(2, '0'));
                if (control is Button)
                {
                    Button button = (Button)control;
                    button.Text = "";
                    button.BackColor = panel1.BackColor;
                }
            }
           wylosujRozwiazanie();
        }

        private void wylosujRozwiazanie()
        {
            timer.Start();
            ustawTablice(tablica);
            kopiuj(tablica, temp);
            Random rand = new Random();
            while(ilosc < 10)
            {
                x = rand.Next(0, 9);
                y = rand.Next(0, 9);
                int liczba = rand.Next(1, 9);
                if(tablica[x,y] == -2)
                {
                    if (SprawdzPoprawnosc(tablica, liczba, x, y))
                    {
                        tablica[x, y] = liczba;
                        ilosc++;
                    }
                    else
                        continue;
                }
            }
            pom = new int[81];
            int a = 0;
            Rozwiaz(tablica);
            for(int i = 0; i < 9; i++)
            {
                for(int j = 0; j < 9; j++)
                {
                    pom[a] = tablica[i, j];
                    a++;
                }
            }
    
            ilosc = 0;
            while (ilosc < poziom)
            {
                int x = rand.Next(0, 9);
                int y = rand.Next(0, 9);
                if (temp[x, y] == -2)
                {
                    if (SprawdzPoprawnosc(temp, tablica[x, y], x, y))
                    {
                        temp[x, y] = tablica[x, y];
                        ilosc++;
                    }
                    else
                        continue;
                }
            }
            ilosc = 0;
               if(SprawdzRozwiazanie(temp))
               {

                while (ilosc < poziom)
                {
                    foreach (Button but in this.panel1.Controls)
                    {
                        int l = rand.Next(80);
                        if (but.TabIndex == l && but.Text == "")
                        {
                            but.Text = pom[l].ToString();
                            but.BackColor = Color.Yellow;
                            kolory[but.TabIndex] = 2;
                            ilosc++;
                            break;
                        }
                    }
                }
               }
               else
               {
                    wylosujRozwiazanie();
               }
            

        }
        private void kopiuj(int[,] wzorzec, int[,] kopia)
        {
            for (int i = 0; i < rozmiar; i++)
            {
                for (int j = 0; j < rozmiar; j++)
                {
                    kopia[i, j] = wzorzec[i, j];
                }
            }
        }
        private bool SprawdzRozwiazanie(int[,] temp)
        {
            if (rozwiazKrokPoKroku(temp))
                return true;
            else
                return false;
        }

        private Boolean rozwiazKrokPoKroku(int[,] kopia)
        {
            int ile;
            int zapamietaj = 0;
            bool czyJest = true;
            while (czyJest)
            {
                czyJest = false;
                for (int i = 0; i < rozmiar; i++)
                {
                    for (int j = 0; j < rozmiar; j++)
                    {
                        if (kopia[i, j] == -2)
                        {
                            ile = 0;
                            for (int a = 1; a < 10; a++)
                            {
                                if (SprawdzPoprawnosc(kopia, dostepne[a - 1], i, j))
                                {
                                    zapamietaj = a;
                                    ile++;
                                }
                            }
                            if (ile == 1)
                            {
                                kopia[i, j] = dostepne[zapamietaj - 1];
                                czyJest = true;
                                ile = 0;
                            }
                        }
                    }
                }
            }
            if (full(kopia))
                return true;
            else
                return false;
        }
        private Boolean full(int[,] tab)
        {
            for (int i = 0; i < rozmiar; i++)
            {
                for (int j = 0; j < rozmiar; j++)
                {
                    if (tab[i, j] == -2)
                        return false;
                }
            }
            return true;
        }

        public static bool Rozwiaz(int[,] tablica)
        {

            if (tablica == null || tablica.GetLength(0) < 9 || tablica.GetLength(1) < 9)
            {
                return false;
            }

            return RozwiazSudoku(tablica, 0, 0);
        }

        private static bool RozwiazSudoku(int[,] tablica, int wiersz, int kolumna)
        {
            if (wiersz > 8)
            {
                return true;
            }

            var odwiedzony = tablica[wiersz, kolumna];
            var isDot = odwiedzony == -2;

            var nextW = kolumna == 8 ? (wiersz + 1) : wiersz;
            var nextC = kolumna == 8 ? 0 : (kolumna + 1);

            if (!isDot)
            {
                return RozwiazSudoku(tablica, nextW, nextC);
            }

            var dostepne = pobierzDostepne(tablica, wiersz, kolumna);

            foreach (var liczba in dostepne)
            {
                tablica[wiersz, kolumna] = liczba;

                var wynik = RozwiazSudoku(tablica, nextW, nextC);

                if (wynik)
                {
                    return true;
                }

                tablica[wiersz, kolumna] = -2;
            }

            return false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            wstaw(button1);
        }


        private void wstaw(Button button)
        {
            if (button.BackColor != Color.Yellow)
            {
                this.panel2.Visible = true;
                b = button;
            }
            else
                this.panel2.Visible = false;
        }
        private void wstawLiczbe(Button button)
        {
            b.Text = button.Text;
            this.panel2.Visible = false;
            if (czyWygrana())
                Zapis();
        }

        private void Zapis()
        {
            timer.Stop();
            StreamWriter sw;
            sw = File.AppendText(@"Wyniki.txt");
            if (podpowiedz)
            {
                MessageBox.Show("Gratulacje! Twój czas: " + textBox1.Text + ", poziom trudności: " + comboBox1.Text + " Uzyto podpowiedzi", "Oszust!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                sw.WriteLine(Form2.name + ", wynik: " + textBox1.Text + " - " + comboBox1.Text + "   OSZUST !!!   ");
            }
          //      MessageBox.Show("Gratulacje! Twój czas: " + textBox1.Text + ", poziom trudności: " + comboBox1.Text + " Uzyto podpowiedzi", "Oszust!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
            {
                MessageBox.Show("Gratulacje! Twój czas: " + textBox1.Text + ", poziom trudności: " + comboBox1.Text, "Zwycięstwo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                sw.WriteLine(Form2.name + ", wynik: " + textBox1.Text + " - " + comboBox1.Text);
            }
              
            sw.Close();
            Application.Exit();
        }

        private bool czyWygrana()
        {
            bool zmienna = false;
            foreach(Button b in this.panel1.Controls)
            {
                if (b.Text == "")
                    return false;
            }
            zmienna = true;
            if (zmienna)
            {
                foreach (Button b in this.panel1.Controls)
                {
                    if (b.Text != pom[b.TabIndex].ToString())
                    {
                        MessageBox.Show("Błędne rozwiązanie", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }
            }
            return true;
        }

        private void button83_Click(object sender, EventArgs e)
        {
            wstawLiczbe(button83);
        }

        private void button86_Click(object sender, EventArgs e)
        {
            wstawLiczbe(button86);
        }

        private void button85_Click(object sender, EventArgs e)
        {
            wstawLiczbe(button85);
        }

        private void button84_Click(object sender, EventArgs e)
        {
            wstawLiczbe(button84);
        }

        private void button89_Click(object sender, EventArgs e)
        {
            wstawLiczbe(button89);
        }

        private void button88_Click(object sender, EventArgs e)
        {
            wstawLiczbe(button88);
        }

        private void button87_Click(object sender, EventArgs e)
        {
            wstawLiczbe(button87);
        }

        private void button90_Click(object sender, EventArgs e)
        {
            wstawLiczbe(button90);
        }

        private void button91_Click(object sender, EventArgs e)
        {
            wstawLiczbe(button91);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            wstaw(button2);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            wstaw(button3);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            wstaw(button4);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            wstaw(button5);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            wstaw(button6);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            wstaw(button7);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            wstaw(button8);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            wstaw(button9);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            wstaw(button10);
        }

        private void button11_Click(object sender, EventArgs e)
        {
            wstaw(button11);
        }

        private void button12_Click(object sender, EventArgs e)
        {
            wstaw(button12);
        }

        private void button13_Click(object sender, EventArgs e)
        {
            wstaw(button13);
        }

        private void button14_Click(object sender, EventArgs e)
        {
            wstaw(button14);
        }

        private void button15_Click(object sender, EventArgs e)
        {
            wstaw(button15);
        }

        private void button16_Click(object sender, EventArgs e)
        {
            wstaw(button16);
        }

        private void button17_Click(object sender, EventArgs e)
        {
            wstaw(button17);
        }

        private void button18_Click(object sender, EventArgs e)
        {
            wstaw(button18);
        }

        private void button19_Click(object sender, EventArgs e)
        {
            wstaw(button19);
        }

        private void button20_Click(object sender, EventArgs e)
        {
            wstaw(button20);
        }

        private void button21_Click(object sender, EventArgs e)
        {
            wstaw(button21);
        }

        private void button22_Click(object sender, EventArgs e)
        {
            wstaw(button22);
        }

        private void button23_Click(object sender, EventArgs e)
        {
            wstaw(button23);
        }

        private void button24_Click(object sender, EventArgs e)
        {
            wstaw(button24);
        }

        private void button25_Click(object sender, EventArgs e)
        {
            wstaw(button25);
        }

        private void button26_Click(object sender, EventArgs e)
        {
            wstaw(button26);
        }

        private void button27_Click(object sender, EventArgs e)
        {
            wstaw(button27);
        }

        private void button28_Click(object sender, EventArgs e)
        {
            wstaw(button28);
        }

        private void button29_Click(object sender, EventArgs e)
        {
            wstaw(button29);
        }

        private void button30_Click(object sender, EventArgs e)
        {
            wstaw(button30);
        }

        private void button31_Click(object sender, EventArgs e)
        {
            wstaw(button31);
        }

        private void button32_Click(object sender, EventArgs e)
        {
            wstaw(button32);
        }

        private void button33_Click(object sender, EventArgs e)
        {
            wstaw(button33);
        }

        private void button34_Click(object sender, EventArgs e)
        {
            wstaw(button34);
        }

        private void button35_Click(object sender, EventArgs e)
        {
            wstaw(button35);
        }

        private void button36_Click(object sender, EventArgs e)
        {
            wstaw(button36);
        }

        private void button37_Click(object sender, EventArgs e)
        {
            wstaw(button37);
        }

        private void button38_Click(object sender, EventArgs e)
        {
            wstaw(button38);
        }

        private void button39_Click(object sender, EventArgs e)
        {
            wstaw(button39);
        }

        private void button40_Click(object sender, EventArgs e)
        {
            wstaw(button40);
        }

        private void button41_Click(object sender, EventArgs e)
        {
            wstaw(button41);
        }

        private void button42_Click(object sender, EventArgs e)
        {
            wstaw(button42);
        }

        private void button43_Click(object sender, EventArgs e)
        {
            wstaw(button43);
        }

        private void button44_Click(object sender, EventArgs e)
        {
            wstaw(button44);
        }

        private void button45_Click(object sender, EventArgs e)
        {
            wstaw(button45);
        }

        private void button46_Click(object sender, EventArgs e)
        {
            wstaw(button46);
        }

        private void button47_Click(object sender, EventArgs e)
        {
            wstaw(button47);
        }

        private void button48_Click(object sender, EventArgs e)
        {
            wstaw(button48);
        }

        private void button49_Click(object sender, EventArgs e)
        {
            wstaw(button49);
        }

        private void button50_Click(object sender, EventArgs e)
        {
            wstaw(button50);
        }

        private void button51_Click(object sender, EventArgs e)
        {
            wstaw(button51);
        }

        private void button52_Click(object sender, EventArgs e)
        {
            wstaw(button52);
        }

        private void button53_Click(object sender, EventArgs e)
        {
            wstaw(button53);
        }

        private void button54_Click(object sender, EventArgs e)
        {
            wstaw(button54);
        }

        private void button55_Click(object sender, EventArgs e)
        {
            wstaw(button55);
        }

        private void button56_Click(object sender, EventArgs e)
        {
            wstaw(button56);
        }

        private void button57_Click(object sender, EventArgs e)
        {
            wstaw(button57);
        }

        private void button58_Click(object sender, EventArgs e)
        {
            wstaw(button58);
        }

        private void button59_Click(object sender, EventArgs e)
        {
            wstaw(button59);
        }

        private void button60_Click(object sender, EventArgs e)
        {
            wstaw(button60);
        }

        private void button61_Click(object sender, EventArgs e)
        {
            wstaw(button61);
        }

        private void button62_Click(object sender, EventArgs e)
        {
            wstaw(button62);
        }

        private void button63_Click(object sender, EventArgs e)
        {
            wstaw(button63);
        }

        private void button64_Click(object sender, EventArgs e)
        {
            wstaw(button64);
        }

        private void button65_Click(object sender, EventArgs e)
        {
            wstaw(button65);
        }

        private void button66_Click(object sender, EventArgs e)
        {
            wstaw(button66);
        }

        private void button67_Click(object sender, EventArgs e)
        {
            wstaw(button67);
        }

        private void button68_Click(object sender, EventArgs e)
        {
            wstaw(button68);
        }

        private void button69_Click(object sender, EventArgs e)
        {
            wstaw(button69);
        }

        private void button70_Click(object sender, EventArgs e)
        {
            wstaw(button70);
        }

        private void button71_Click(object sender, EventArgs e)
        {
            wstaw(button71);
        }

        private void button72_Click(object sender, EventArgs e)
        {
            wstaw(button72);
        }

        private void button73_Click(object sender, EventArgs e)
        {
            wstaw(button73);
        }

        private void button74_Click(object sender, EventArgs e)
        {
            wstaw(button74);
        }

        private void button75_Click(object sender, EventArgs e)
        {
            wstaw(button75);
        }

        private void button76_Click(object sender, EventArgs e)
        {
            wstaw(button76);
        }

        private void button77_Click(object sender, EventArgs e)
        {
            wstaw(button77);
        }

        private void button78_Click(object sender, EventArgs e)
        {
            wstaw(button78);
        }

        private void button79_Click(object sender, EventArgs e)
        {
            wstaw(button79);
        }

        private void button80_Click(object sender, EventArgs e)
        {
            wstaw(button80);
        }

        private void button82_Click(object sender, EventArgs e)
        {
            wstaw(button82);
        }

        private void button92_Click(object sender, EventArgs e)
        {
            podpowiedz = true;
            Random random = new Random();
            bool tak = true;
            while (tak)
            {
                int liczba = random.Next(81);
                foreach (Button but in this.panel1.Controls)
                {
                    if (but.TabIndex == liczba && but.Text == "")
                    {
                        but.Text = pom[liczba].ToString();
                        tak = false;
                        if (czyWygrana())
                            Zapis();
                        else
                          break;
                    }
                }

            }

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBox1.Text == "łatwy")
            {
                poziom = 50;
            }
            else if(comboBox1.Text == "średni")
            {
                poziom = 44;
            }
            else if(comboBox1.Text == "trudny")
            {
                poziom = 39;
            }
        }

        private void button93_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 f2 = new Form2();
            f2.ShowDialog();
        }

        private void button94_Click(object sender, EventArgs e)
        {
            string nazwa = Form2.name;
            string godz = h.ToString();
            string min = m.ToString();
            string sec = s.ToString();
            string poziomdoZapisu = comboBox1.Text;
            string[] doZapisu = new string[81];
            foreach(Button b in this.panel1.Controls)
            {
                if (b.Text == "")
                    doZapisu[b.TabIndex] = "0";
                else
                    doZapisu[b.TabIndex] = b.Text;
            }
            StreamWriter sw = new StreamWriter("Zapis.txt");
            sw.WriteLine(nazwa);
            for(int i = 0; i<kolory.Length; i++)
            {
                if (kolory[i] == 2)
                    sw.Write(i + ",");
            }
            sw.WriteLine();
            for(int i = 0; i < pom.Length; i++)
            {
                sw.Write(pom[i] +",");
            }
            sw.WriteLine();
            sw.WriteLine(godz);
            sw.WriteLine(min);
            sw.WriteLine(sec);
            sw.Write(poziomdoZapisu);
            for(int i = 0; i < doZapisu.Length; i++)
            {
                if (i % 9 == 0)
                {
                    sw.WriteLine();
                    sw.Write(doZapisu[i]);
                }
                else
                    sw.Write(doZapisu[i]);
            }
            sw.Close();
        }

        private static HashSet<int> pobierzDostepne(int[,] tablica, int wiersz, int kolumna)
        {
            int[] liczby = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            var zwrocDostepne = new HashSet<int>(liczby);

            for (int col = 0; col < 9; col++)
            {
                var odwiedzony = tablica[wiersz, col];
                var isDigit = odwiedzony != -2 ;

                if (isDigit)
                {
                    zwrocDostepne.Remove(odwiedzony);
                }
            }

            for (int row = 0; row < 9; row++)
            {
                var odwiedzony = tablica[row, kolumna];
                var isDigit = odwiedzony != -2;

                if (isDigit)
                {
                    zwrocDostepne.Remove(odwiedzony);
                }
            }

            var startW = wiersz / 3 * 3;
            var startC = kolumna / 3 * 3;
            for (int w = startW; w < startW + 3; w++)
            {
                for (int c = startC; c < startC + 3; c++)
                {
                    var odwiedzony = tablica[w, c];
                    var isDigit = odwiedzony != -2;

                    if (isDigit)
                    {
                        zwrocDostepne.Remove(odwiedzony);
                    }
                }
            }

            return zwrocDostepne ;
        }
        public static Boolean SprawdzPoprawnosc(int[,] tablica, int wartosc, int obecneX, int obecneY)
            {
                int wiersz = (obecneX / 3) * 3;
                int kolumna = (obecneY / 3) * 3;

                for (int i = 0; i < 9; i++)
                {
                    if (tablica[obecneX, i] == wartosc)
                    {
                        return false;
                    }
                    if (tablica[i, obecneY] == wartosc)
                    {
                        return false;
                    }
                    if (tablica[wiersz + (i % 3), kolumna + (i / 3)] == wartosc)
                    {
                        return false;
                    }
                }
                return true;
            }
        

        private void ustawTablice(int[,] tablica)
        {
            for(int i=0; i<9; i++)
            {
                for(int j=0; j<9; j++)
                {
                    tablica[i, j] = -2;
                }
            }
        }
        private void zeruj(int[] tab)
        {
            for (int i = 0; i < 81; i++)
                tab[i] = 0;
        }
    }
}
